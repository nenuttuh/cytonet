import sqlalchemy as sa
import unittest
import numpy as np
from sqlalchemy.ext.declarative import declarative_base
from cytonet import testbenches
from cytonet.metrics import multilabel_conf_mat
from modeltb import models, settings


class TestTestbenches(unittest.TestCase):

    def setUp(self):
        settings.load_test_settings()
        # settings.engine = sa.create_engine('sqlite:///:memory:')
        models.Base.metadata.create_all(settings.engine)
        self.session = settings.Session()

    def tearDown(self):
        models.Base.metadata.drop_all(settings.engine)

    def test_multilabel_conf_mat_saving(self):
        y_true = np.array([np.zeros(13)])
        y_pred = np.array([np.zeros(13)])
        y_true[0][3] = 1
        y_pred[0][5] = 1
        cm = multilabel_conf_mat(y_true, y_pred)
        tb = testbenches.Major13TB()
        tb._cm = cm.tostring()
        self.session.add(tb)
        self.session.commit()
        read_back_tb = self.session.query(testbenches.CytoTB).all()[0]
        read_back_cm = read_back_tb.get_multilabel_cm(as_dataframe=False)
        self.assertTrue((cm == read_back_cm).all())
