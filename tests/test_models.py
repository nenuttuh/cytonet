import unittest

from modeltb import models as modeltb_models
from modeltb import settings
from cytonet import models


class TestModels(unittest.TestCase):

    def setUp(self):
        settings.load_test_settings()
        modeltb_models.Base.metadata.create_all(settings.engine)
        self.session = settings.Session()

    def tearDown(self):
        modeltb_models.Base.metadata.drop_all(settings.engine)

    def test_activation_functions(self):
        m1 = models.CytoCNNv2()
        m2 = models.CytoCNNv2(activation='elu')

        self.assertEqual(m1.activation, 'relu')
        self.assertEqual(m2.activation, 'elu')
