import unittest
import os
import cytonet.imageinfo


class TestImageinfo(unittest.TestCase):

    def test_get_img_info(self):
        xml_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'data',
            'miniatlas.xml'
        )
        hpa_names = ['570_F9_2', '584_F9_1']

        info = cytonet.imageinfo.get_img_info(hpa_names, xml_path)

        expected = {
            '570_F9_2': {'cell_line': 'A-431',
                'verification': 'approved',
                'locations': ['nucleoplasm', 'endoplasmic reticulum'],
                'img_url': 'http://v17.proteinatlas.org/images/38664/570_F9_2_blue_red_green.jpg'},
            '584_F9_1': {'cell_line': 'U-251 MG',
                'verification': 'approved',
                'locations': ['nucleoplasm', 'endoplasmic reticulum'],
                'img_url': 'http://v17.proteinatlas.org/images/38664/584_F9_1_blue_red_green.jpg'}
        }

        self.assertEqual(info, expected)


if __name__ == '__main__':
    unittest.main()
