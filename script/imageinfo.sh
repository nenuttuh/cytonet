#!/bin/bash
set -e


function print_help {
  printf "Usage:\n\timageinfo -x ATLAS_PATH -d IMAGE_DIR -i CYTO_GT_PATH [-f OUTPUT_FILE]\n"
  exit 0
}


while getopts ":hx:d:f:i:" opt; do
  case ${opt} in
    h ) print_help
      ;;
    x ) atlasfile=$OPTARG
      ;;
    d ) imgdir=$OPTARG
      ;;
    f ) outfile=$OPTARG
      ;;
    i ) gtfile=$OPTARG
      ;;
    \? ) print_help
      ;;
    : ) echo "Invalid option: $OPTARG requires an argument" 1>&2
      ;;
  esac
done
shift $((OPTIND -1))


script_root="$(dirname "$(readlink -f "$0")")"


# Create imginfo dir
imginfodir="$(dirname "$imgdir")/$(basename $imgdir)_imginfo"
echo "Creating imginfodir into $imginfodir"
if ! [ -d "$imginfodir" ]; then
  mkdir -p "$imginfodir"
fi


# Create cyto_names.txt and hpa_names.txt into imginfo dir
cd $imgdir
echo "Creating hpa_names.txt..."
gunzip -l --name *_green.tif.gz |
	sed 's/\(.*\)_green.tif/\1/' |
	sed 's/\(.*\)_z0_ch0[0-3].tif/\1/' |
	awk '{print $4}' |
	head -n-1 |
	tail -n+2 > "$imginfodir/hpa_names.txt"


echo "Creating cyto_names.txt..."
gunzip -l *_green.tif.gz |
	sed 's/\(.*\)_green.tif/\1/' |
	awk '{print $4}' |
	head -n-1 |
	tail -n+2 > "$imginfodir/cyto_names.txt"


# If outfile is not defined, define it.
if [ -z "$outfile" ]; then
  outfile="$imginfodir/$(basename $imgdir)_imginfo.csv"
fi
echo "Saving output into $outfile"


# Check that the atlas.xml file exists
if ! [ -f "$atlasfile" ]; then
  echo "Error: given atlasfile $atlasfile does not exist!" 1>&2
  exit 1
fi


# Call the python script to do the magic
cd $script_root
python3 ../cytonet/imageinfo.py --atlasfile $atlasfile --imginfodir $imginfodir --gtfile $gtfile --outfile $outfile

