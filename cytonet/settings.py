import os
import errno
from configparser import ConfigParser


DEFAULT_CONFIG = """
[cytonet]
data_dir = {data_dir}
"""

if 'CYTONET_HOME' not in os.environ:
    CYTONET_HOME = os.path.expanduser('~/cytonet')
else:
    CYTONET_HOME = os.path.expanduser(os.path.expandvars(os.environ['CYTONET_HOME']))


if 'CYTONET_DEFAULT_DATA_DIR' not in os.environ:
    CYTONET_DEFAULT_DATA_DIR = os.path.join(CYTONET_HOME, 'data')
else:
    CYTONET_DEFAULT_DATA_DIR = os.path.expanduser(
            os.path.expandvars(os.environ['CYTONET_DEFAULT_DATA_DIR']))


CYTONET_CONFIG_FILE = os.path.join(CYTONET_HOME, 'cytonet.cfg')


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise ModeltbConfigException('Could not create directory')


mkdir_p(CYTONET_HOME)


if not os.path.isfile(CYTONET_CONFIG_FILE):
    with open(CYTONET_CONFIG_FILE, 'w') as f:
        config_str = DEFAULT_CONFIG.format(
                data_dir=CYTONET_DEFAULT_DATA_DIR)
        f.write(config_str)


config = ConfigParser()
config.read(CYTONET_CONFIG_FILE)
CYTONET_DATA_DIR = config['cytonet']['data_dir']
mkdir_p(os.path.join(CYTONET_DATA_DIR, 'models'))
