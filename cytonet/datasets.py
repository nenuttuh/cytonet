import pandas as pd
import os
import ast
from modeltb import models as base_models
from sklearn.model_selection import train_test_split
from cytonet.settings import config
from sqlalchemy import Column, Boolean, ForeignKey, Integer, Text


def get_dataframe(dataset='major13'):    
    csv_path = os.path.join(
            config['cytonet']['data_dir'],
            '%s_imginfo' % dataset,
            '%s_imginfo.csv' % dataset)
    df = pd.read_csv(csv_path, sep='\t', index_col=0)
    return df


class Major13Dataset(base_models.Dataset):
    """Dataset that returns samples from cyto2017 major13 dataset."""
    id = Column(Integer, ForeignKey('dataset.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'major13dataset'}

    def train_test_split(self, test_size=0.1, seed=None):
        """Create train and test sets, save as csv files and return their paths.

        Args:
            seed: An integer seed for RNG. When the seed is same, the datasets are the same too.

        Returns:
            (train_csv_path, test_csv_path): the csv paths containing training and testing splits.
        """
        major13 = get_dataframe()
        train, test = train_test_split(major13, test_size=test_size, random_state=seed)
        return (train, test)


class FixedMajor13DS(base_models.Dataset):
    """Major13 dataset with test set fixed to 4k last samples.
    I.e. test cyto_labels in range(16001, 20001)"""
    id = Column(Integer, ForeignKey('dataset.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'fixedmajor13ds'}

    # Save the training cell line filter as text
    _training_cell_lines = Column(Text)

    def __init__(self, training_cell_lines=None, **kwargs):
        """Major13 dataset with test samples fixed to 4k last ones.

        Args:
            training_cell_lines: a list of cell line names to be included in
                training data
        """
        self._training_cell_lines = str(training_cell_lines)
        super().__init__(**kwargs)

    @property
    def training_cell_lines(self):
        return ast.literal_eval(self._training_cell_lines)

    def train_test_split(self, test_size=0.1, seed=None):
        major13 = get_dataframe()

        test = major13[major13['cyto_name'] > 16000]
        train = major13[major13['cyto_name'] <= 16000]
        if self.training_cell_lines is not None:
            train = train[train['cell_line'].isin(self.training_cell_lines)]

        return (train, test)


class FilteredCellLineDS(base_models.Dataset):
    """A Dataset that has a filter for selecting the cell lines of interest."""
    id = Column(Integer, ForeignKey('dataset.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'filteredcelllineds'}

    _cell_lines = Column(Text)
    single_label = Column(Boolean(name='single_label_bool'), default=False)

    def __init__(self, cell_lines=None, single_label=False, **kwargs):
        self._cell_lines = str(cell_lines)
        self.single_label = single_label
        super().__init__(**kwargs)

    @property
    def cell_lines(self):
        return ast.literal_eval(self._cell_lines)

    def train_test_split(self, test_size=0.2, seed=None):
        df = get_dataframe()
        # Filter by cell lines
        if self.cell_lines is not None:
            df = df[df['cell_line'].isin(self.cell_lines)]
        # Filter single-label, i.e. keep only samples that have only one location
        if self.single_label:
            df[df['locations'].apply(lambda loc: len(ast.literal_eval(loc)) == 1)]

        train, test = train_test_split(df, test_size=test_size, random_state=seed)
        return (train, test)
