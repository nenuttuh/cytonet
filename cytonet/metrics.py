import numpy as np


def multilabel_conf_mat(y_true, y_pred):
    """Confusion matrix for multi-class multi-label case.

    Note that this is not correct by definition.
    """
    assert y_true.shape == y_pred.shape, "y_true.shape should equal y_pred.shape"
    n_classes = y_true.shape[1]
    cm = np.zeros((n_classes, n_classes), dtype=int)

    for y_tr, y_pr in zip(y_true, y_pred):
        idx_true = np.where(y_tr==1)[0]
        idx_pred = np.where(y_pr==1)[0]

        for t in idx_true:
            if t in idx_pred:
                cm[t, t] += 1
            else:
                cm[t, idx_pred] += 1
    return cm
