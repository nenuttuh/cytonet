from modeltb.models import Testbench
from modeltb.settings import Session
from modeltb.utils.db import provide_session
from cytonet.models import *
from cytonet.testbenches import *


TB_IDS = range(54)


@provide_session
def fill_missing_cm(tb_id, recompute=False, session=None):
    """Load the testbench and model, predict labels, calculate per-class
    confusion matrices and store to db.
    """
    print("Starting confusion matrix calculation for tb id %s" % tb_id)
    tb = session.query(Testbench).filter(Testbench.id == tb_id).one()
    if tb._tp is not None and not recompute:
        print("Confusion matrix for tb id %s exists already. Skipping." % tb_id)
        return None

    _, test = tb.dataset.train_test_split(tb.test_size, tb.seed)
    y_pred = tb.model.predict(test)
    tp, tn, fp, fn = tb.get_confusion_matrices(y_pred, test)
    tb._tp = str(tp)
    tb._tn = str(tn)
    tb._fp = str(fp)
    tb._fn = str(fn)
    print("Confusion matrix fill for tb id %s succeeded" % tb_id)


def fill_missing_cms(tb_ids, discard_errors=True):
    for tb_id in tb_ids:
        try:
            fill_missing_cm(tb_id)
        except Exception as e:
            print('Error filling confusion matrices for testbench id %s' % tb_id)
            if discard_errors:
                print(e)
            else:
                raise


if __name__ == '__main__':
    fill_missing_cms(TB_IDS)
