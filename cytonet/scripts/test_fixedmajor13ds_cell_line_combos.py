import json
import numpy as np
from modeltb.utils.db import provide_session
from cytonet.testbenches import Major13TB
from cytonet.models import CytoCNNv2, CytoFCN


AUGMENTATION_CONFIG = {
    'channel_shift_range': 0.0,
    'cval': 0.0,
    'data_format': 'channels_last',
    'featurewise_center': False,
    'featurewise_std_normalization': False,
    'fill_mode': 'nearest',
    'height_shift_range': 0.2,
    'horizontal_flip': True,
    'preprocessing_function': None,
    'rotation_range': 0.0,
    'samplewise_center': True,
    'samplewise_std_normalization': True,
    'shear_range': 0.2,
    'vertical_flip': True,
    'width_shift_range': 0.2,
    'zca_epsilon': 1e-06,
    'zca_whitening': False,
    'zoom_range': 0.2
}

BATCH_SIZE = 64


@provide_session
def create_tb(tb_cls, model_cls, training_cell_lines=None,
        n_epochs=100, model_params={}, session=None):
    tb = tb_cls(training_cell_lines=training_cell_lines)
    tb.slurm = True
    tb.gpu = True

    model = model_cls(**model_params)
    model.batch_size = BATCH_SIZE
    model.n_epochs = n_epochs
    model._augmentation_config = json.dumps(AUGMENTATION_CONFIG)

    tb.model = model
    session.add(model)
    session.add(tb)


def calc_n_epochs(n_train, n_train_all=16000, n_epochs_all=100):
    """Calculate how many epochs should be run for n_train samples
    so that the amount of training would approximately equal to
    running n_epochs_all epochs for n_train_all data, where each epoch
    goes through the full training data set.
    """
    return int(np.floor(n_epochs_all / (n_train / n_train_all)))


def create_cell_line_combo_tbs(n_epochs_all=100):
    """n_epochs_all is the number of epochs for testbench including all
    cell lines in training data.
    """
    # For cell line U-2 OS
    cell_lines = ['U-2 OS']
    n_train = 5083
    # Make n_epochs proportionate to number of training samples...
    # For example, U-2 OS is about third of the data, so n_epochs is 3 x n_epochs_all
    create_tb(Major13TB, CytoCNNv2, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))
    create_tb(Major13TB, CytoFCN, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))

    # For cell line A-431
    cell_lines = ['A-431']
    n_train = 3472
    create_tb(Major13TB, CytoCNNv2, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))
    create_tb(Major13TB, CytoFCN, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))

    # For cell line U-251 MG
    cell_lines = ['U-251 MG']
    n_train = 3200
    create_tb(Major13TB, CytoCNNv2, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))
    create_tb(Major13TB, CytoFCN, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))

    # For top-3 cell lines
    cell_lines = ['U-2 OS', 'A-431', 'U-251 MG']
    n_train = 11756
    create_tb(Major13TB, CytoCNNv2, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))
    create_tb(Major13TB, CytoFCN, training_cell_lines=cell_lines,
            n_epochs=calc_n_epochs(n_train, n_epochs_all=n_epochs_all))

    # For all cell lines
    create_tb(Major13TB, CytoCNNv2)
    create_tb(Major13TB, CytoFCN)

    # Try out ELU too
    create_tb(Major13TB, CytoCNNv2, model_params={'activation': 'elu'})
    create_tb(Major13TB, CytoFCN, model_params={'activation': 'elu'})


if __name__ == '__main__':
    create_cell_line_combo_tbs()

