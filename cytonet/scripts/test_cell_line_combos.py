import json
from modeltb.utils.db import provide_session
from cytonet.testbenches import U2OSMajor13TB, A431Major13TB, U251MGMajor13TB, \
        Top3CLMajor13TB
from cytonet.models import CytoCNNv2, CytoFCN


AUGMENTATION_CONFIG = {
    'channel_shift_range': 0.0,
    'cval': 0.0,
    'data_format': 'channels_last',
    'featurewise_center': False,
    'featurewise_std_normalization': False,
    'fill_mode': 'nearest',
    'height_shift_range': 0.2,
    'horizontal_flip': True,
    'preprocessing_function': None,
    'rotation_range': 0.0,
    'samplewise_center': True,
    'samplewise_std_normalization': True,
    'shear_range': 0.2,
    'vertical_flip': True,
    'width_shift_range': 0.2,
    'zca_epsilon': 1e-06,
    'zca_whitening': False,
    'zoom_range': 0.2
}

BATCH_SIZE = 64
N_EPOCHS = 100


@provide_session
def create_tb(tb_cls, model_cls, session=None):
    tb = tb_cls()
    tb.slurm = True
    tb.gpu = True

    model = model_cls()
    model.batch_size = BATCH_SIZE
    model.n_epochs = N_EPOCHS
    model._augmentation_config = json.dumps(AUGMENTATION_CONFIG)

    tb.model = model
    session.add(model)
    session.add(tb)


def create_cell_line_combo_tbs():
    # For cell line U-2 OS
    create_tb(U2OSMajor13TB, CytoCNNv2)
    create_tb(U2OSMajor13TB, CytoFCN)

    # For cell line A-431
    create_tb(A431Major13TB, CytoCNNv2)
    create_tb(A431Major13TB, CytoFCN)

    # For cell line U-251 MG
    create_tb(U251MGMajor13TB, CytoCNNv2)
    create_tb(U251MGMajor13TB, CytoFCN)

    # For top-3 cell lines
    create_tb(Top3CLMajor13TB, CytoCNNv2)
    create_tb(Top3CLMajor13TB, CytoFCN)


if __name__ == '__main__':
    create_cell_line_combo_tbs()

