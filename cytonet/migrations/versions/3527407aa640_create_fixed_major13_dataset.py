"""create fixed major13 dataset

Revision ID: 3527407aa640
Revises: 18a726bc2189
Create Date: 2018-02-08 12:49:17.475241

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3527407aa640'
down_revision = '18a726bc2189'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('fixedmajor13ds',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['id'], ['dataset.id'], name=op.f('fk_fixedmajor13ds_id_dataset')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_fixedmajor13ds'))
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('fixedmajor13ds')
    # ### end Alembic commands ###
