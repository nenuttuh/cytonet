"""Add mask_size and base_model_type to CytoFCN

Revision ID: 2344815cea24
Revises: 8893c65ac537
Create Date: 2018-03-07 17:14:03.171468

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2344815cea24'
down_revision = '8893c65ac537'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('cytofcn', schema=None) as batch_op:
        batch_op.add_column(sa.Column('_mask_size', sa.String(length=20), nullable=True))
        batch_op.add_column(sa.Column('base_model_type', sa.String(length=30), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('cytofcn', schema=None) as batch_op:
        batch_op.drop_column('base_model_type')
        batch_op.drop_column('_mask_size')

    # ### end Alembic commands ###
