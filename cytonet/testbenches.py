import numpy as np
import pandas as pd
import ast
import logging
from modeltb import models as base_models
from modeltb.utils.db import provide_session
from sklearn.metrics import confusion_matrix, f1_score
from cytonet import datasets, models
from cytonet.utils import data_generators as data_gen
from cytonet.metrics import multilabel_conf_mat
from sqlalchemy import Column, Integer, ForeignKey, LargeBinary, Text


logger = logging.getLogger(__name__)


def labels_to_vec(df, classes, label_col='cyto_labels'):
    """Transform lists of class label names to vectors of 0 or 1."""
    y = np.zeros((len(df), len(classes)))
    class_index = {k: i for i, k in enumerate(classes)}
    for i, labels in enumerate(df['cyto_labels']):
        idx = [class_index[label] for label in ast.literal_eval(labels)]
        y[i, idx] = 1.
    return y


class CytoTB(base_models.Testbench):
    """A base class for all cytonet testbenches."""
    id = Column(Integer, ForeignKey('testbench.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'cytotb'}

    _per_class_scores = Column(Text)
    _tp = Column(Text)
    _tn = Column(Text)
    _fp = Column(Text)
    _fn = Column(Text)
    _cm = Column(LargeBinary)

    def __init__(self, model=None, dataset=None, 
            seed=42, test_size=0.1, **kwargs):
        super().__init__(model=model, dataset=dataset, seed=seed,
                test_size=test_size, **kwargs)

    @classmethod
    def get_score(cls, y_pred, test_df):
        """Return weighted average of class F1 scores."""
        # This is major13 test bench, so all 13 classes are included
        classes = cls.get_classes()
        y_true = labels_to_vec(test_df, classes)
        return f1_score(y_true, y_pred, average='weighted')

    @classmethod
    def get_per_class_scores(cls, y_pred, test_df):
        classes = cls.get_classes()
        y_true = labels_to_vec(test_df, classes)
        per_class = [f1_score(y_true[:, i], y_pred[:, i]) for i in range(
            len(classes))]
        return per_class

    @classmethod
    def get_confusion_matrices(cls, y_pred, test_df):
        classes = cls.get_classes()
        y_true = labels_to_vec(test_df, classes)
        confmats = np.array(
                [confusion_matrix(y_true[:, i], y_pred[:, i]) for i in range(len(classes))])
        tp = confmats[:, 1, 1]
        tn = confmats[:, 0, 0]
        fp = confmats[:, 0, 1]
        fn = confmats[:, 1, 0]

        return (list(tp), list(tn), list(fp), list(fn))

    @provide_session
    def save_results(self, y_pred, test_data, session=None):
        """Save extra info on testbench results."""
        logger.info("Major13TB saving results for Testbench %s" % self.id)
        # Get the test data from data generator. Define batch size to be the
        # size of the dataset, so that all data will be returned by gen.next()
        session.add(self.model)
        test_img_gen = data_gen.CytoImageDataGenerator(
                rescale=self.model.rescale,
                samplewise_center=self.model.augmentation_config['samplewise_center'],
                samplewise_std_normalization=self.model.augmentation_config[
                    'samplewise_std_normalization'])

        iter_params = self.model.iterator_params
        iter_params['batch_size'] = 2000
        iter_params['shuffle'] = False
        iter_params['classes'] = self.get_classes()
        test_gen = test_img_gen.flow_from_df(test_data,
                self.model.image_dir,
                **iter_params)
        x, y = test_gen.next()

        # Run evaluation
        metrics = self.model.model.evaluate(x, y)

        # Save as TBMetric objects
        for name, value in zip(self.model.model.metrics_names, metrics):
            logger.info("Test %s: %s" % (name, value))
            metric = base_models.TBMetric(name=name, value=value, testbench=self)
            session.add(metric)

        per_class_scores = self.get_per_class_scores(y_pred, test_data)
        self._per_class_scores = str(per_class_scores)
        tp, tn, fp, fn = self.get_confusion_matrices(y_pred, test_data)
        self._tp = str(tp)
        self._tn = str(tn)
        self._fp = str(fp)
        self._fn = str(fn)

        y_true = labels_to_vec(test_data, self.get_classes())
        cm = multilabel_conf_mat(y_true, y_pred)
        self._cm = cm.tostring()

        session.add(self)

    @property
    def per_class_scores(self):
        return ast.literal_eval(self._per_class_scores)

    @property
    def tp(self):
        if self._tp is not None:
            return ast.literal_eval(self._tp)
        else:
            return 'null'

    @property
    def tn(self):
        if self._tn is not None:
            return ast.literal_eval(self._tn)
        else:
            return 'null'

    @property
    def fp(self):
        if self._fp is not None:
            return ast.literal_eval(self._fp)
        else:
            return 'null'

    @property
    def fn(self):
        if self._fp is not None:
            return ast.literal_eval(self._fn)
        else:
            return 'null'

    def get_multilabel_cm(self, as_dataframe=True):
        """Return the confusion matrix as np.array or pd.Dataframe."""
        classes = self.get_classes()
        if self._cm is None:
            return None
        else:
            cm = np.fromstring(self._cm, dtype=int).reshape((len(classes), len(classes)))

        if as_dataframe:
            cm = pd.DataFrame(cm, columns=classes, index=classes)

        return cm


class Major13TB(CytoTB):
    """Testbench for testing with single-class samples in major13 dataset."""
    id = Column(Integer, ForeignKey('cytotb.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'major13tb'}

    def __init__(self, model=None, seed=42, test_size=0.1,
            training_cell_lines=None, **kwargs):
        """Initialize with Major13Dataset."""
        dataset = datasets.FixedMajor13DS(training_cell_lines=training_cell_lines)
        super().__init__(seed=seed,
                test_size=test_size,
                model=model,
                dataset=dataset,
                **kwargs)

    @classmethod
    def get_classes(cls):
        return models.MAJOR_CLASSES


class U2OSMajor13TB(CytoTB):
    id = Column(Integer, ForeignKey('cytotb.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'u2osmajor13tb'}

    def __init__(self, model=None, seed=42, test_size=0.2, **kwargs):
        dataset = datasets.FilteredCellLineDS(cell_lines=['U-2 OS'])
        super().__init__(seed=seed, test_size=test_size, model=model,
                dataset=dataset, **kwargs)

    @classmethod
    def get_classes(cls):
        return models.MAJOR_CLASSES


class A431Major13TB(CytoTB):
    id = Column(Integer, ForeignKey('cytotb.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'a431major13tb'}

    def __init__(self, model=None, seed=42, test_size=0.2, **kwargs):
        dataset = datasets.FilteredCellLineDS(cell_lines=['A-431'])
        super().__init__(seed=seed, test_size=test_size, model=model,
                dataset=dataset, **kwargs)

    @classmethod
    def get_classes(cls):
        return models.MAJOR_CLASSES


class U251MGMajor13TB(CytoTB):
    id = Column(Integer, ForeignKey('cytotb.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'u251mgmajor13tb'}

    def __init__(self, model=None, seed=42, test_size=0.2, **kwargs):
        dataset = datasets.FilteredCellLineDS(cell_lines=['U-251 MG'])
        super().__init__(seed=seed, test_size=test_size, model=model,
                dataset=dataset, **kwargs)

    @classmethod
    def get_classes(cls):
        return models.MAJOR_CLASSES


class Top3CLMajor13TB(CytoTB):
    id = Column(Integer, ForeignKey('cytotb.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'top3clmajor13tb'}

    def __init__(self, model=None, seed=42, test_size=0.2, **kwargs):
        dataset = datasets.FilteredCellLineDS(cell_lines=['U-2 OS', 'A-431', 'U-251 MG'])
        super().__init__(seed=seed, test_size=test_size, model=model,
                dataset=dataset, **kwargs)

    @classmethod
    def get_classes(cls):
        return models.MAJOR_CLASSES
