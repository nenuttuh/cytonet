import logging
import ast
import json
import os
import numpy as np
from modeltb import models as base_models
from modeltb.utils.db import provide_session
from modeltb.exceptions import ModeltbException
from cytonet import settings
from cytonet.utils.data_generators import CytoImageDataGenerator
from cytonet.testbenches import labels_to_vec
from keras import backend as K
from keras.models import model_from_json, Sequential, Model
from keras.models import load_model as keras_load_model
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.layers import Activation, BatchNormalization, Conv2D, Dense, Dropout, Flatten, \
        GlobalAveragePooling2D, Input, MaxPooling2D, Conv2DTranspose
from keras.applications.inception_v3 import InceptionV3
from keras.applications.xception import Xception
from keras.applications.resnet50 import ResNet50
from keras import optimizers
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sqlalchemy import Boolean, Column, Enum, Float, ForeignKey, Integer, \
        String, Text


logger = logging.getLogger(__name__)


class EpochMetricHistory(Callback):
    """A custom callback that will save epoch metrics to db."""
    def on_train_begin(self, logs={}):
        self.losses = {}
        self.accs = {}
        self.val_losses = {}
        self.val_accs = {}

    def on_epoch_end(self, epoch, logs={}):
        # Save metrics into dicts where key is the epoch number
        self.losses[epoch] = logs.get('loss')
        self.accs[epoch] = logs.get('acc')

        # Val_loss may be None if no validation is configured
        val_loss = logs.get('val_loss')
        if val_loss is not None:
            self.val_losses[epoch] = val_loss

        # Same that with val_loss
        val_acc = logs.get('val_acc')
        if val_acc is not None:
            self.val_accs[epoch] = val_acc

    @provide_session
    def save_to_db(self, model, session=None):
        losses = [base_models.EpochMetric(name='loss', value=v, epoch=e, model=model)
                for e, v in self.losses.items()]
        accs = [base_models.EpochMetric(name='acc', value=v, epoch=e, model=model)
                for e, v in self.accs.items()]
        val_losses = [base_models.EpochMetric(name='val_loss', value=v, epoch=e, model=model)
                for e, v in self.val_losses.items()]
        val_accs = [base_models.EpochMetric(name='val_acc', value=v, epoch=e, model=model)
                for e, v in self.val_accs.items()]

        session.add_all(losses)
        session.add_all(accs)
        session.add_all(val_losses)
        session.add_all(val_accs)
        session.commit()


# Labels found in each dataset_type
DATASET_TYPES = ('major13', 'mito_nui', 'rare_events')
MAJOR_CLASSES = (
        'Actin filaments',
        'Centrosome',
        'Cytosol',
        'Endoplasmic reticulum',
        'Golgi apparatus',
        'Intermediate filaments',
        'Microtubules',
        'Mitochondria',
        'Nuclear membrane',
        'Nucleoli',
        'Nucleus',
        'Plasma membrane',
        'Vesicles')
MITO_NUI_CLASSES = ('Mitochondria', 'Nucleoli')
RARE_EVENTS_CLASSES = MAJOR_CLASSES + (
        'Aggresome',
        'Cytokinetic bridge',
        'Focal adhesion sites',
        'Microtubule organizing center',
        'Nuclear speckles',
        'Nucleoli fibrillar center')
DATASET_CLASSES = {
    'major13': tuple(sorted(MAJOR_CLASSES)),
    'mito_nui': tuple(sorted(MITO_NUI_CLASSES)),
    'rare_events': tuple(sorted(RARE_EVENTS_CLASSES))
}

# All params and their defaults used for CytoImageDataGenerator augmentation.
# By default, no augmentation is performed. See the parameter definitions in
# cytonet.utils.data_generators.CytoImageDataGenerator
DEFAULT_AUGMENTATION_CONFIG = {
    'featurewise_center': False,
    'samplewise_center': False,
    'featurewise_std_normalization': False,
    'samplewise_std_normalization': False,
    'zca_whitening': False,
    'zca_epsilon': 1e-6,
    'rotation_range': 0.,
    'width_shift_range': 0.,
    'height_shift_range': 0.,
    'shear_range': 0.,
    'zoom_range': 0.,
    'channel_shift_range': 0.,
    'fill_mode': 'nearest',
    'cval': 0.,
    'horizontal_flip': False,
    'vertical_flip': False,
    'preprocessing_function': None,
    'data_format': K.image_data_format()
}


def _single_best_f1_thr(y_true, y_proba):
    # 0.01, 0.02, ..., 0.89, 0.90
    thresholds = np.linspace(0.01, 0.9, 90)
    best_f1 = f1_score(y_true, (y_proba > thresholds[0]).astype(float))
    best_thr = thresholds[0]

    for thr in thresholds[1:]:
        score = f1_score(y_true, (y_proba > thr).astype(float))
        if score > best_f1:
            best_f1 = score
            best_thr = thr

    return best_thr


def calculate_best_f1_thr(y_true, y_proba):
    """Calculate thresholds for classifying with f1 score as metric."""
    n_classes = y_true.shape[1]
    best_thr = np.zeros(n_classes)

    # If we are dealing with FCN, reduce the activations to the max val
    # of each activation map.
    if len(y_proba.shape) > 2:
        y_proba = np.max(y_proba, axis=(1, 2))

    for i in range(0, n_classes):
        best_thr[i] = _single_best_f1_thr(y_true[:, i], y_proba[:, i])

    return best_thr


class CytoModel(base_models.Model):
    """A base model used for the experimentation with cyto challenge data.
    
    Parameterized so that it can be used with hyperparam optimization.
    """
    id = Column(Integer, ForeignKey('model.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'cytomodel'}

    # Define all data we want to save to db as columns here
    _augmentation_config = Column(Text)
    batch_size = Column(Integer)
    dataset_type = Column(Enum('major13', 'mito_nui', 'rare_events', name='dataset_type_enum'))
    dropout = Column(Float)
    _channels = Column(String(50))
    _classes = Column(Text)
    class_mode = Column(String(32))
    epoch_shuffle = Column(Boolean(name='epoch_shuffle_bool'))
    _input_shape = Column(String(32))
    interpolation = Column(String(50))
    loss = Column(String(50))
    n_epochs = Column(Integer) 
    optimizer = Column(String(50))
    rescale = Column(Float)
    seed = Column(Integer)
    target_height = Column(Integer)
    target_width = Column(Integer)
    _best_thresholds = Column(Text)
    squash_to_rgb = Column(Boolean(name='squash_to_rgb_bool'))

    # The model configuration as a JSON string (only architecture)
    # If the model is saved with self.save_model, the model is saved into
    # a file located in self.model_path
    model_config = Column(Text)

    def __init__(self,
            seed=None,
            batch_size=64,
            channels=('red', 'green', 'blue', 'yellow'),
            classes=None,
            class_mode='categorical',
            dropout=0.5,
            epoch_shuffle=False,
            loss='binary_crossentropy',
            n_epochs=10,
            optimizer='adam',
            rescale=1./255,  # Rescale from [0, 255] to [0, 1]
            target_size=(256, 256),
            dataset_type='major13',
            augmentation_config=DEFAULT_AUGMENTATION_CONFIG,
            interpolation='nearest',
            squash_to_rgb=False,
            activation='relu'):
        # Attributes saved to separate db columns
        self._augmentation_config = json.dumps(augmentation_config)
        self.batch_size = batch_size
        self.dataset_type = dataset_type
        self.dropout = dropout
        self._channels = str(channels)
        self.class_mode = class_mode
        self.epoch_shuffle = epoch_shuffle
        if squash_to_rgb:
            self._input_shape = str(target_size + (3,))
        else:
            self._input_shape = str(target_size + (len(channels),))
        self.interpolation = interpolation
        self.loss = loss
        self.n_epochs = n_epochs
        self.optimizer = optimizer
        self.rescale = rescale
        self.seed = seed
        self.target_height = target_size[0]
        self.target_width = target_size[1]
        self.squash_to_rgb = squash_to_rgb
        self.activation = activation

        # If classes param is not given, set the classes according to dataset_type
        if classes is None:
            classes = DATASET_CLASSES[self.dataset_type]
        self._classes = str(classes)
        self.n_classes = len(classes)

        # Finally, call _init_model() which will instantiate the model
        # architecture, and preferably save it to self.model_config
        # with Keras utility model.to_json()
        self._init_model()

    def _init_model(self):
        """To try out different structures, inherit from this class,
        and override _init_model()
        """
        raise NotImplementedError(
            "_init_model needs to be implemented when inheriting from CytoModel")

    @property
    def target_size(self):
        return (self.target_height, self.target_width)

    # NOTE: quite many things are internally stored to database as strings.
    # This is done e.g. with tuples and dicts.
    # They have corresponding @property definitions, which return the things
    # as Python objects.
    @property
    def channels(self):
        return ast.literal_eval(self._channels)

    @property
    def classes(self):
        return ast.literal_eval(self._classes)

    @property
    def input_shape(self):
        return ast.literal_eval(self._input_shape)

    @property
    def augmentation_config(self):
        return json.loads(self._augmentation_config)

    @property
    def best_thresholds(self):
        return np.asarray(ast.literal_eval(self._best_thresholds))

    @property
    def image_dir(self):
        # Images should be found in CYTONET_DATA_DIR/<dataset_type>, e.g.
        # /home/user/data/cyto2017/major13
        return os.path.join(settings.CYTONET_DATA_DIR, self.dataset_type)

    def load_model_architecture(self):
        """Load the model's architecture from self.model_config."""
        if self.model_config is None:
            raise ModeltbException("Model %s with id %s does not have model_config." % (
                self.__class__.__name__,
                self.id))
        self.model = model_from_json(self.model_config)

    @property
    def model_path(self):
        model_file = "%s_%s.h5" % (self.__class__.__name__.lower(), self.id)
        return os.path.join(settings.CYTONET_DATA_DIR, 'models', model_file)

    @property
    def iterator_params(self):
        """The mutual parameters for train and validation for
        CytoDataFrameIterator.
        """
        return {
            'target_size': self.target_size,
            'classes': self.classes,
            'class_mode': self.class_mode,
            'batch_size': self.batch_size,
            'shuffle': self.epoch_shuffle,
            'seed': self.seed,
            'interpolation': self.interpolation,
            'img_format': 'png',
            'channels': self.channels,
            'squash_to_rgb': self.squash_to_rgb
        }

    def load_model(self):
        """Load the whole model from file."""
        self.model = keras_load_model(self.model_path)

    def save_model(self):
        """Save the whole model into a file."""
        self.model.save(self.model_path)

    def get_optimizer(self):
        if self.optimizer == 'adam':
            # return optimizers.Adam(amsgrad=True)
            return optimizers.Adam()

    def fit(self, train_df, validation_size=0.1, class_weight=None):
        """Use ImageDataGenerator to provide batches of training images and fit the model.

        The pandas DataFrame in train_df includes the names and labels for training images.
        This set can be further splitted into train and validation sets.
        Save epoch metrics on each epoch.
        """
        # Compute class weights to be used with fit_generator
        if class_weight == 'balanced':
            label_vec = labels_to_vec(train_df, self.classes)
            # See
            # http://scikit-learn.org/stable/modules/generated/sklearn.utils.class_weight.compute_class_weight.html
            weights = len(label_vec) / (len(self.classes) *
                    np.sum(label_vec, axis=0) + len(label_vec)/2.0)
            cw = {i: v for i, v in enumerate(weights)}
        else:
            cw = None

        # Create train and validation generators
        train_img_gen = CytoImageDataGenerator(
                rescale=self.rescale,
                **self.augmentation_config
                )
        validation_img_gen = CytoImageDataGenerator(
                rescale=self.rescale,
                samplewise_center=self.augmentation_config['samplewise_center'],
                samplewise_std_normalization=self.augmentation_config['samplewise_std_normalization'])

        train_data, validation_data = train_test_split(
                train_df,
                test_size=validation_size,
                random_state=self.testbench.seed)

        logger.info("%s id %s: start training with %s training samples and %s validation samples" %
                (self.__class__.__name__, self.id, len(train_data), len(validation_data)))

        train_gen = train_img_gen.flow_from_df(train_data, self.image_dir,
                **self.iterator_params)
        if validation_size:
            validation_gen = validation_img_gen.flow_from_df(validation_data,
                    self.image_dir, **self.iterator_params)
        else:
            validation_gen = None
        
        # Load and compile the model
        if not hasattr(self, 'model'):
            self.load_model_architecture()
        self.model.compile(optimizer=self.get_optimizer(), loss=self.loss, metrics=['accuracy'])

        # Define the callbacks for fit()
        model_checkpoint = ModelCheckpoint(self.model_path, save_best_only=True, verbose=1)
        early_stopping = EarlyStopping(patience=20, verbose=1)
        reduce_lr = ReduceLROnPlateau(factor=0.33, patience=8, verbose=1)
        epoch_history = EpochMetricHistory()

        # Train the model
        hist = self.model.fit_generator(
                train_gen,
                validation_data=validation_gen,
                epochs=self.n_epochs,
                steps_per_epoch=len(train_data)//self.batch_size,
                validation_steps=len(validation_data)//self.batch_size,
                callbacks=[epoch_history, early_stopping, model_checkpoint, reduce_lr],
                class_weight=cw)

        # Calculate best classify thresholds with all training data.
        # TODO: try with just the validation set
        # Reload the model first, so that the model used in threshold calculation will be
        # the one with best validation loss...
        self.load_model()
        y_true = labels_to_vec(train_df, self.classes)
        y_proba = self.predict(train_df, classify=False)
        best_thr = calculate_best_f1_thr(y_true, y_proba)
        self._best_thresholds = str(list(best_thr))

        # Save the model so that it can be used in testbench performance assessment
        # TODO: Should the model then be automatically removed if not configured otherwise?
        # NOTE: changed to use ModelCheckpoint which takes care of the saving!
        # self.save_model()

        # Save the epoch metrics into db. Get the model id first because the model
        # will be detached from session here.
        model_id = self.id
        epoch_history.save_to_db(model=self)

        logger.info("CytoCNN.fit: ended training model id %d Training history: %s" % (
                model_id,
                hist.history))
        return self

    def predict(self, test_df, classify=True, fill_negative=True):
        """Load model if not loaded and predict on batches.

        Args:
            test_df: A pandas.DataFrame including the test image cyto names.
            classify: Whether to return 0s and 1s instead of the class probabilities.
            classify_thr: Threshold for classification. If probability >= classify_thr,
                the class is present in the sample.
        Returns:
            Class probabilities if classify=False, classifications otherwise
        """
        # TODO: Raise exception (?) if model is not trained
        if not hasattr(self, 'model'):
            self.load_model()

        logger.info("%s id %s: starting prediction with %s samples" %
                (self.__class__.__name__, self.id, len(test_df)))

        iter_params = self.iterator_params
        iter_params['class_mode'] = None
        iter_params['shuffle'] = False

        # Create a generator that feeds the images to model
        test_img_gen = CytoImageDataGenerator(rescale=self.rescale,
                samplewise_center=self.augmentation_config['samplewise_center'],
                samplewise_std_normalization=self.augmentation_config['samplewise_std_normalization'])
        test_gen = test_img_gen.flow_from_df(test_df, self.image_dir,
                **iter_params)

        # Predict with generator and return predictions as ndarray.
        y_proba = self.model.predict_generator(test_gen)
        if classify:
            y_pred = self._classify(y_proba, self.best_thresholds, fill_negative)
            return y_pred

        return y_proba

    @staticmethod
    def _classify(y_proba, thresholds, fill_negative=True):
        # If the model is FCN, need to reduct dimensionality of the predictions.
        # ATM, just pick the max probability of each activation map.
        if len(y_proba.shape) > 2:
            y_proba = np.max(y_proba, axis=(1,2))

        y_pred = y_proba >= thresholds
        y_pred = y_pred.astype(float)

        if fill_negative:
            # If all zeros, predict the highest one, because all samples belong
            # to at least one class
            positive_idx_set = set(np.unique(np.where(y_pred > 0.0)[0]))
            # Indices with all negatives are those that do not belong in positive_idx_set
            negative_idx_set = set(range(0, len(y_pred))) - positive_idx_set
            negative_idx = list(negative_idx_set)
            for i in negative_idx:
                max_idx = y_proba[i].argmax()
                y_pred[i][max_idx] = 1.0

        return y_pred


class CytoCNN(CytoModel):
    """A Parameterized CNN implementation for cyto challenge data."""
    id = Column(Integer, ForeignKey('cytomodel.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'cytocnn'}

    def __init__(self,
            conv_layers=([(16, (3, 3)), (16, (3, 3)), (32, (3, 3))]),
            **kwargs):
        # Network structure parameters get saved to model_config json string
        self.conv_layers = conv_layers

        super().__init__(**kwargs)

    def _init_model(self):
        model = Sequential()

        for i, (n_filters, kernel_size) in enumerate(self.conv_layers):
            # For the first layer define the input shape
            conv_name = "conv_%d" % i
            if i == 0:
                model.add(Conv2D(n_filters, kernel_size, input_shape=self.input_shape,
                    padding='same', name=conv_name))
            else:
                model.add(Conv2D(n_filters, kernel_size, padding='same', name=conv_name))
            model.add(Activation('relu', name='conv_act_%d' % i))
            # TODO: Add optionally another conv -> relu before pooling
            model.add(MaxPooling2D(pool_size=(2, 2), name='conv_pool_%d' % i))
            # if self.dropout:
            #    model.add(Dropout(self.dropout))

        # Add fully connected layers to the end
        model.add(Flatten())
        model.add(Dense(128))
        model.add(Activation('relu'))
        model.add(Dropout(self.dropout))
        model.add(Dense(self.n_classes))
        model.add(Activation('sigmoid'))

        self.model = model

        # Save the model configuration into db
        self.model_config = model.to_json()


class CytoCNNv2(CytoModel):
    """A Parameterized CNN implementation for cyto challenge data."""
    id = Column(Integer, ForeignKey('cytomodel.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'cytocnnv2'}

    def __init__(self,
            conv_layers=([(32, (3, 3)), (64, (3, 3)), (128, (3, 3)),
                (256, (3, 3)), (256, (3, 3))]),
            **kwargs):
        # Network structure parameters get saved to model_config json string
        self.conv_layers = conv_layers

        super().__init__(**kwargs)

    def _init_model(self):
        model = Sequential()

        for i, (n_filters, kernel_size) in enumerate(self.conv_layers):
            # For the first layer define the input shape
            conv_name = "conv_%d" % i
            if i == 0:
                model.add(Conv2D(n_filters, kernel_size, input_shape=self.input_shape,
                    padding='same', name=conv_name))
            else:
                model.add(Conv2D(n_filters, kernel_size, padding='same', name=conv_name))
            model.add(Activation(self.activation, name='conv_act_%d' % i))

            model.add(Conv2D(n_filters, kernel_size, padding='same',
                name=conv_name + '_2'))
            model.add(Activation(self.activation, name='conv_act_%d_2' % i))
            #TODO: Dropout on conv layers?
            model.add(BatchNormalization())

            model.add(MaxPooling2D(pool_size=(2, 2), name='conv_pool_%d' % i))

        # Add fully connected layers to the end
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation(self.activation))
        model.add(Dropout(self.dropout))

        model.add(Dense(512))
        model.add(Activation(self.activation))
        model.add(Dropout(self.dropout))

        model.add(Dense(self.n_classes))
        model.add(Activation('sigmoid'))

        self.model = model

        # Save the model configuration into db
        self.model_config = model.to_json()


class CytoInception(CytoModel):
    """A fine-tuned Inception V3 model.

    Args:
        fc_layer_size: The number of neurons in the hidden FC layer
        n_freezed_layers: The number of bottom layers to freeze when fine-tuning
        n_stage1_epochs: The number of epochs to train the final layers before fine-tuning
        squash_to_rgb: Whether to use all four channels or just the rgb. If all are used,
            the y channel will be added to r and b channels. G will be intact, because
            it contains the protein activation intensity.
        optimizer_params: A dict containing the parameters. E.g. {lr: 0.001}
    """
    id = Column(Integer, ForeignKey('cytomodel.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'cytoinception'}

    fc_layer_size = Column(Integer)
    n_freezed_layers = Column(Integer)
    n_stage1_epochs = Column(Integer)
    base_model_class = Column(String(30))

    # The optimizer params as a JSON string
    _optimizer_params = Column(Text)

    def __init__(self, fc_layer_size=1024, n_freezed_layers=0,
            optimizer_params=None, n_stage1_epochs=1,
            squash_to_rgb=True, base_model_class='inceptionv3',
            **kwargs):
        self.fc_layer_size = fc_layer_size
        self.n_freezed_layers = n_freezed_layers
        self._optimizer_params = json.dumps(optimizer_params)
        self.n_stage1_epochs = n_stage1_epochs
        self.base_model_class = base_model_class

        kwargs['squash_to_rgb'] = squash_to_rgb
        if base_model_class == 'resnet50':
            kwargs['target_size'] = (224, 224)
        else:
            kwargs['target_size'] = (299, 299)
        # With larger batch size, Xception and resnet50 will result in OOM
        # on a gpu with 11 GB of memory available
        kwargs['batch_size'] = 32

        super().__init__(**kwargs)

    @property
    def optimizer_params(self):
        return json.loads(self._optimizer_params)

    def _init_model(self):
        # Load the base model
        if self.base_model_class == 'inceptionv3':
            base_class = InceptionV3
        elif self.base_model_class == 'xception':
            base_class = Xception
        elif self.base_model_class == 'resnet50':
            base_class = ResNet50
        else:  # Unknown base_model_class
            raise ModeltbException("CytoInception: unknown base_model_class %s" %
                    self.base_model_class)
        base_model = base_class(weights='imagenet', include_top=False)

        # Add a global average pooling layer, FC layer and the final class score layer
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        x = Dense(self.fc_layer_size, activation='relu')(x)
        predictions = Dense(self.n_classes, activation='sigmoid')(x)

        # This is the model that is compiled and  trained
        model = Model(inputs=base_model.input, outputs=predictions)

        # Freeze base model layers
        for layer in base_model.layers:
            layer.trainable = False

        # Save the model structure
        self.model = model
        self.model_config = model.to_json()

    def _create_optimizer(self):
        """Create the optimizer for fine tuning."""
        if self.optimizer_params is None:
            # If there are no specified params, just use defaults
            return self.optimizer
        else:
            if self.optimizer == 'sgd':
                return optimizers.SGD(**self.optimizer_params)
            elif self.optimizer == 'adam':
                return optimizers.Adam(**self.optimizer_params)
            else:  # Unknown optimizer, raise error
                raise ModeltbException(
                        "%s._create_optimizer() id %s: Unknown optimizer type %s with parameters %s" %
                        (self.__class__.__name__, self.id, self.optimizer, self.optimizer_params))

    def fit(self, train_df, validation_size=0.1):
        """First, fit the custom top layers. Then, unfreeze some of the
        convolutional layers and fit again to fine tune."""
        # Create train and validation generators
        train_img_gen = CytoImageDataGenerator(
                rescale=self.rescale,
                **self.augmentation_config
                )
        validation_img_gen = CytoImageDataGenerator(
                rescale=self.rescale,
                samplewise_center=self.augmentation_config['samplewise_center'],
                samplewise_std_normalization=self.augmentation_config['samplewise_std_normalization'])

        train_data, validation_data = train_test_split(
                train_df,
                test_size=validation_size,
                random_state=self.testbench.seed)

        logger.info("%s id %s: start training with %s training samples and %s validation samples" %
                (self.__class__.__name__, self.id, len(train_data), len(validation_data)))

        train_gen = train_img_gen.flow_from_df(train_data, self.image_dir,
                **self.iterator_params)
        if validation_size:
            validation_gen = validation_img_gen.flow_from_df(validation_data,
                    self.image_dir, **self.iterator_params)
        else:
            validation_gen = None

        # Load and compile the model
        if not hasattr(self, 'model'):
            self.load_model_architecture()

        # First, train with the base_model's layers freezed
        self.model.compile(optimizer='adam', loss=self.loss, metrics=['accuracy'])

        # Train on the new data for  n_stage1_epochs
        epoch_history = EpochMetricHistory()
        hist = self.model.fit_generator(
                train_gen,
                validation_data=validation_gen,
                epochs=self.n_stage1_epochs,
                steps_per_epoch=len(train_data)//self.batch_size,
                validation_steps=len(validation_data)//self.batch_size,
                callbacks=[epoch_history])

        # Unfreeze some of the base_model's layers
        for layer in self.model.layers[:self.n_freezed_layers]:
            layer.trainable = False
        for layer in self.model.layers[self.n_freezed_layers:]:
            layer.trainable = True

        # Build the optimizer for fine tuning and recompile the model
        optimizer = self._create_optimizer()
        self.model.compile(optimizer=optimizer, loss=self.loss, metrics=['accuracy'])

        # Fine tune
        hist = self.model.fit_generator(
                train_gen,
                validation_data=validation_gen,
                epochs=self.n_epochs + self.n_stage1_epochs,
                steps_per_epoch=len(train_data)//self.batch_size,
                initial_epoch=self.n_stage1_epochs,
                validation_steps=len(validation_data)//self.batch_size,
                callbacks=[epoch_history])

        # Calculate best classify thresholds with all training data.
        y_true = labels_to_vec(train_df, self.classes)
        y_proba = self.predict(train_df, classify=False)
        best_thr = calculate_best_f1_thr(y_true, y_proba)
        self._best_thresholds = str(list(best_thr))

        # Save the model so that it can be used in testbench performance assessment
        self.save_model()

        # Save the epoch metrics into db. Get the model id first because the model
        # will be detached from session here.
        model_id = self.id
        epoch_history.save_to_db(model=self)

        logger.info("%s.fit: ended training model id %d Training history: %s" % (
                self.__class__.__name__,
                model_id,
                hist.history))
        return self


class CytoFCN(CytoModel):
    """A CNN without fully connected layer in the end. The FCN has as a separate filter
    for each class label in the final layer, i.e. per-class activation maps.
    """
    id = Column(Integer, ForeignKey('cytomodel.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'cytofcn'}

    # Mask size tuple stored as a string, e.g. '(16, 16)'
    _mask_size = Column(String(20))
    # Base model type, defaults to 'custom'
    base_model_type = Column(String(30))

    #TODO:
    # 1. DONE: Initialize the model to a proper size
    # 2. DONE: Implement support for class_mode 'masks' into data generators
    #   - Gaussian filter -> threshold -> dilate -> downsample to output size
    #   - One mask per class. Negative classes all zero. Positive classes equal the mask.
    # 3. DONE: Threshold calculation for masks
    # 4. DONE: Classification with thresholds for masks
    # 5. Customise optimizer creation in CytoModel as in CytoInception
    #   - In the same effort, change the default params for augmentation and stuff to
    #     better defaults!
    def __init__(self,
            conv_layers=([(32, (3, 3)), (64, (3, 3)), (128, (3, 3)),
                (256, (3, 3)), (256, (3, 3))]),
            n_deconv_layers=0,
            deconv_kernel_size=(4, 4),
            base_model_type='custom',
            **kwargs):
        self.conv_layers = conv_layers
        self.n_deconv_layers = n_deconv_layers
        self.deconv_kernel_size = deconv_kernel_size
        self.base_model_type = base_model_type
        kwargs['class_mode'] = 'masks'

        # Pretrained models trained with ImageNet require 3-channel input
        if base_model_type in ('inceptionv3, xception, resnet'):
            kwargs['squash_to_rgb'] = True

        super().__init__(**kwargs)

    def _init_model(self):
        base_model = self._get_base_model(base_model_type=self.base_model_type)
        x = base_model.output

        # Add the transposed convolution layers to upsample the output
        # TODO: Test zero-padding instead of 'same'. For this, use ZeroPadding2D
        if self.n_deconv_layers > 0:
            # TODO: Should the deconv layers have activation at all? Seems that in the
            # long2014 paper there is no activation defined for the deconv outputs...
            activation = self.activation
            x = Conv2D(self.n_classes, (1, 1), padding='same', activation=activation)(x)
            for i  in range(self.n_deconv_layers):
                # For the last layer, change activation to sigmoid
                if i == self.n_deconv_layers - 1:
                    activation = 'sigmoid'
                x = Conv2DTranspose(self.n_classes, self.deconv_kernel_size, strides=(2, 2),
                        padding='same', activation=activation)(x)
            predictions = x
        else:
            predictions = Conv2D(self.n_classes, (1, 1), padding='same', activation='sigmoid')(x)
        model = Model(inputs=base_model.input, outputs=predictions)

        self.model = model
        self.model_config = model.to_json()
        self._mask_size = str(self.model.output_shape[1:-1])

    def _get_base_model(self, base_model_type='custom'):
        """Build and return the FCN base model before the final layers.

        With this, we can test different model structures within the same CytoFCN class.

        Args:
            base_model_type: the base structure to be used. Following are possible:
                'custom': build a user-defined model using the initial parameters, e.g. conv_layers
                'inceptionv3': use inception v3 as base
                'xception': Xception based network
                'resnet': ResNet based network

        Returns:
            a keras Model instance that can be extended with the common FCN layer(s)
        """
        input_tensor = Input(shape=self.input_shape)

        if base_model_type == 'custom': 
            for i, (n_filters, kernel_size) in enumerate(self.conv_layers):
                if i == 0:
                    x = Conv2D(n_filters, kernel_size, padding='same',
                            activation=self.activation)(input_tensor)
                else:
                    x = Conv2D(n_filters, kernel_size, padding='same',
                            activation=self.activation)(x)

                x = Conv2D(n_filters, kernel_size, padding='same',
                        activation=self.activation)(x)
                x = BatchNormalization()(x)
                # Do not add maxpooling to the final layer
                if i != len(self.conv_layers) - 1:
                    x = MaxPooling2D(pool_size=(2, 2))(x)

            return Model(inputs=input_tensor, outputs=x)
                 
        if base_model_type == 'inceptionv3':
            base_model_class = InceptionV3
        elif base_model_type == 'xception':
            base_model_class = Xception
        elif base_model_type == 'resnet':
            base_model_class = ResNet
        else:
            raise NotImplementedError("%s: base_model_type %s not implemented" %
                    (self.__class__.__name__, base_model_type))

        return base_model_class(input_tensor=input_tensor, weights='imagenet', include_top=False)

    @property
    def mask_size(self):
        return ast.literal_eval(self._mask_size)

    @property
    def iterator_params(self):
        """The mutual parameters for train and validation for
        CytoDataFrameIterator.
        """
        return {
            'target_size': self.target_size,
            'classes': self.classes,
            'class_mode': self.class_mode,
            'batch_size': self.batch_size,
            'shuffle': self.epoch_shuffle,
            'seed': self.seed,
            'interpolation': self.interpolation,
            'img_format': 'png',
            'channels': self.channels,
            'squash_to_rgb': self.squash_to_rgb,
            'mask_size': self.mask_size
        }


class CombinedCytoNet(object):
    """A CNN with auxiliary output of the activation maps."""
    pass
