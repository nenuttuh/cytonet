import numpy as np
import pandas as pd
import logging
import os
import ast
import time
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator, Iterator
from keras import backend as K
from skimage.io import imread as skimage_imread
from skimage.transform import resize as skimage_resize
from skimage.morphology import binary_dilation, disk
from skimage.filters import gaussian


logger = logging.getLogger(__name__)


def load_img(path, target_size=None, interpolation='nearest'):
    """Load image to PIL format."""
    if image.pil_image is None:
        raise ImportError("Could not import PIL.image")
    
    # Some of the images are in a RGB TIFF format that PIL.Image.open
    # throws an OSError from. But skimage.io.imread can read them, so use
    # it on those cases.
    try:
        img = image.pil_image.open(path)
        # Most of the images in cyto data are in 16-bit format.
        # Change the format into 8-bit so that further transformations
        # will function correctly.
        if img.mode == 'I;16':
            img = img.point(lambda i: i * (1./255)).convert('L')
        elif img.mode == 'P':
            # Do nothing, this mode is 8-bit and will be resized and transformed
            # correctly as is.
            pass
        elif img.mode == 'L':
            pass
        else:
            logger.error("load_img error: unknown mode (%s)" % img.mode)
            raise ValueError("load_img error: unknown mode (%s)" % img.mode)
        if target_size is not None:
            # Height and width seem to be in the opposite order in PIL vs numpy conventions
            width_height_tuple = (target_size[1], target_size[0])
            if img.size != width_height_tuple:
                if interpolation not in image._PIL_INTERPOLATION_METHODS:
                    raise ValueError("load_img: invalid interpolation method: %s"
                            % interpolation)
                resample = image._PIL_INTERPOLATION_METHODS[interpolation]
                img = img.resize(width_height_tuple, resample)
    except OSError:
        # Got the weird format that PIL.Image.open cannot handle...
        img = skimage_imread(path, as_grey=True)
        if target_size is not None:
            img = skimage_resize(img, target_size)
        img = image.array_to_img(img[..., np.newaxis])

    return img


def transform_images(src_dir, target_dir, target_size=(512, 512), target_format='png'):
    """Helper function to save images in a specific size and format."""
    img_names = os.listdir(src_dir)
    for name in img_names:
        img = load_img(os.path.join(src_dir, name), target_size=target_size)
        new_path = os.path.join(target_dir, name.split(sep='.')[0] + '.' + target_format)
        img.save(new_path)


class CytoDataFrameIterator(Iterator):
    """An iterator used by CytoImageDataGenerator to generate batches from samples
    defined by a CSV file.

    Args:
        df: The dataframe containing image names and labels for generating
            batches
        image_dir: The directory where the sample images reside.
        image_data_generator: ImageDataGenerator used for transformations and normalization
        target_size: Tuple of height and width for images to be resized
        classes: Optional list of classes to be included. Will drop samples with
            classes not in this list TODO: really?
        class_mode: Mode for yielding the targets:
            'categorical': each sample's label will be a vector of zeros or ones,
                with one indicating that the class is present in the sample
            'masks': Return masks for each class for FCN training. 
                TODO: How to configure the masking function? Size, morphology, etc...
            None: Do not generate class labels, but only the images
        batch_size: How many samples per batch to generate
        shuffle: Whether to shuffle data between epochs, so that the images are
            generated in different order
        seed: Random seed for shuffling
        save_to_dir: Optional directory where to save the yielded images for
            visualization and debugging.
        interpolation: Interpolation method used to resample the image if target_size
            is different from the original image size. Supported formats are whatever
            PIL version supports. For PIL version =>1.3 at least "nearest",
            "bilinear", "bicubic" and "lanczos" are supported. Default is "nearest".
        img_format: The input image format, e.g. 'png',
        channels: The names of channels to be included in Cyto dataset
    """
    def __init__(self, df, image_dir, image_data_generator,
            target_size=(256, 256),
            classes=None,
            class_mode='categorical',
            data_format=None,
            batch_size=32,
            shuffle=True,
            seed=None,
            save_to_dir=None,
            interpolation='nearest',
            img_format='png',
            channels=('red', 'green', 'blue', 'yellow'),
            squash_to_rgb=False,
            mask_size=None):
        self.df = df
        self.image_dir = image_dir
        self.image_data_generator = image_data_generator
        if data_format is None:
            data_format = K.image_data_format()
        self.data_format = data_format
        self.target_size = tuple(target_size)
        self.squash_to_rgb = squash_to_rgb
        self.mask_size = mask_size
        # Four channels, r, g, b, y, except three if squash_to_rgb
        if squash_to_rgb:
            self.image_shape = self.target_size + (3,)
        else:
            self.image_shape = self.target_size + (len(channels),)

        if classes is None:
            # Create a set of unique class labels in the dataset
            classes = set()
            _ = df['cyto_labels'].apply(
                    lambda locations: [classes.add(l) for l in ast.literal_eval(locations)])

        self.classes = tuple(sorted(classes))
        self.class_index = {k: i for i, k in enumerate(sorted(classes))}

        if class_mode not in ('categorical', 'masks', None):
            raise ValueError("CytoDataFrameIterator: invalid class_mode: %s" % str(class_mode))
        
        self.class_mode = class_mode
        self.save_to_dir = save_to_dir
        self.interpolation = interpolation
        self.channels = channels
        self.img_format = img_format

        # Count the samples in the dataset
        self.samples = len(df)

        # Initialize the Iterator with its args
        super().__init__(self.samples, batch_size, shuffle, seed)

    def _load_cyto_img(self, cyto_name, img_format):
        """Read channels and return as a numpy array."""
        xs = np.zeros(self.image_shape, dtype=K.floatx())
        for i, ch in enumerate(self.channels):
            filename = "%s_%s.%s" % (str(cyto_name), ch, img_format)
            path = os.path.join(self.image_dir, filename)
            img = load_img(path,
                    target_size=self.target_size,
                    interpolation=self.interpolation)
            x = image.img_to_array(img)
            if self.squash_to_rgb:
                # Instead of handling yellow as a separate channel, add it to
                # red and blue.
                if ch in ('red', 'blue'):
                    x = x/2.0
                elif ch == 'yellow':
                    # Add half and half to red and blue channels
                    x = x/2.0
                    xs[..., 0] += x[:, :, 0]
                    xs[..., 2] += x[:, :, 0]
                    # Break out of the loop so that y channel is not set
                    break
            xs[..., i] = x[:, :, 0]

        return xs

    @staticmethod
    def _create_mask(input_array, mask_size, radius=10, threshold=0.3):
        # Scale to [0, 1]
        scaled = (input_array - np.min(input_array)) / (np.max(input_array) - np.min(input_array))
        # Threshold after applying gaussian filter
        thresholded = gaussian(scaled) > threshold
        # Apply binary dilation
        mask = binary_dilation(thresholded, selem=disk(radius))
        # Return resized array. TODO: resize function makes some averaging which
        # leads to non-binary result. Should the masks be binary, or not?
        # For now, just threshold the resized array again and return float
        resized = (skimage_resize(mask, mask_size) > 0.5).astype(K.floatx())
        return resized

    def _get_batches_of_transformed_samples(self, index_array):
        """Read images, transform them if so configured and return with labels.
        
        Args:
            index_array: ???
        """
        logger.debug("Creating a batch of %s samples" % len(index_array))
        start_time = time.time()
        batch_x = np.zeros((len(index_array),) + self.image_shape, dtype=K.floatx())

        if self.class_mode in ('categorical', 'masks'):
            # Get the classes, sort alphabetically and get indices of the categories
            # Probably safest to define the `classes` param in the model, so that
            # the classes will be the same with train, test and validation sets...
            class_label_arr = np.zeros((len(index_array), len(self.classes)))
            for i, labels in enumerate(self.df['cyto_labels'].iloc[index_array]):
                idx = [self.class_index[label] for label in ast.literal_eval(labels)]
                class_label_arr[i, idx] = 1.

        if self.class_mode == 'categorical':
            batch_y = class_label_arr
        elif self.class_mode == 'masks':
            batch_y_shape = (len(index_array),) + self.mask_size + (len(self.classes),)
            batch_y = np.zeros(batch_y_shape, dtype=K.floatx())

        # Build batch of image data
        for i, j in enumerate(index_array):
            cyto_name = self.df['cyto_name'].iloc[j]
            x = self._load_cyto_img(cyto_name, self.img_format)
            x = self.image_data_generator.random_transform(x)
            x = self.image_data_generator.standardize(x)
            batch_x[i] = x

            if self.class_mode == 'masks':
                # Set the mask for each sample belonging to a class to the generated mask.
                # Otherwise, leave to zero.
                mask = self._create_mask(x[:, :, self.channels.index('green')], self.mask_size)
                batch_y[i, :, :, class_label_arr[i].astype(bool)] = mask

        if self.class_mode is None:
            # class_mode is None, return only batches of image data
            stop_time = time.time()
            logger.debug("Batch generation took %s seconds" % str(stop_time-start_time))
            return batch_x

        stop_time = time.time()
        logger.debug("Batch generation took %s seconds" % str(stop_time-start_time))
        return batch_x, batch_y

    def next(self):
        """Return the next batch."""
        with self.lock:
            index_array = next(self.index_generator)
        # The image transformation is not under thread lock,
        # so it can be done in parallel
        return self._get_batches_of_transformed_samples(index_array)


class CytoImageDataGenerator(ImageDataGenerator):
    """A generator that yields batches optionally augmented images infinitely.

    This class only adds function `flow_from_df` to keras ImageDataGenerator.
    Kwargs (taken from ImageDataGenerator docstring):
        featurewise_center: set input mean to 0 over the dataset.
        samplewise_center: set each sample mean to 0.
        featurewise_std_normalization: divide inputs by std of the dataset.
        samplewise_std_normalization: divide each input by its std.
        zca_whitening: apply ZCA whitening.
        zca_epsilon: epsilon for ZCA whitening. Default is 1e-6.
        rotation_range: degrees (0 to 180).
        width_shift_range: fraction of total width, if < 1, or pixels if >= 1.
        height_shift_range: fraction of total height, if < 1, or pixels if >= 1.
        shear_range: shear intensity (shear angle in degrees).
        zoom_range: amount of zoom. if scalar z, zoom will be randomly picked
            in the range [1-z, 1+z]. A sequence of two can be passed instead
            to select this range.
        channel_shift_range: shift range for each channel.
        fill_mode: points outside the boundaries are filled according to the
            given mode ('constant', 'nearest', 'reflect' or 'wrap'). Default
            is 'nearest'.
            Points outside the boundaries of the input are filled according to the given mode:
                'constant': kkkkkkkk|abcd|kkkkkkkk (cval=k)
                'nearest':  aaaaaaaa|abcd|dddddddd
                'reflect':  abcddcba|abcd|dcbaabcd
                'wrap':  abcdabcd|abcd|abcdabcd
        cval: value used for points outside the boundaries when fill_mode is
            'constant'. Default is 0.
        horizontal_flip: whether to randomly flip images horizontally.
        vertical_flip: whether to randomly flip images vertically.
        rescale: rescaling factor. If None or 0, no rescaling is applied,
            otherwise we multiply the data by the value provided. This is
            applied after the `preprocessing_function` (if any provided)
            but before any other transformation.
        preprocessing_function: function that will be implied on each input.
            The function will run before any other modification on it.
            The function should take one argument:
            one image (Numpy tensor with rank 3),
            and should output a Numpy tensor with the same shape.
        data_format: 'channels_first' or 'channels_last'. In 'channels_first' mode,
            the channels dimension
            (the depth) is at index 1, in 'channels_last' mode it is at index 3.
            It defaults to the `image_data_format` value found in your
            Keras config file at `~/.keras/keras.json`.
            If you never set it, then it will be "channels_last".
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def flow_from_df(self, df, image_dir,
            target_size=(256, 256),
            classes=None,
            class_mode='categorical',
            batch_size=32,
            shuffle=True,
            seed=None,
            save_to_dir=None,
            interpolation='nearest',
            img_format='png',
            channels=('red', 'green', 'blue', 'yellow'),
            squash_to_rgb=False,
            mask_size=None):
        return CytoDataFrameIterator(
                df, image_dir, self,
                target_size=target_size,
                classes=classes,
                class_mode=class_mode,
                batch_size=batch_size,
                shuffle=shuffle,
                seed=seed,
                save_to_dir=save_to_dir,
                interpolation=interpolation,
                img_format=img_format,
                channels=channels,
                squash_to_rgb=squash_to_rgb,
                mask_size=mask_size)
