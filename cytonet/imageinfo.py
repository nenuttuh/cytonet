# Utility functions for extracting information on the images.
# Author: Riku Huttunen riku.huttunen@tut.fi

import numpy as np
import pandas as pd
import os
import skimage.io
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import copy
import argparse
from skimage.transform import resize as skimage_resize


def get_one_img_info(hpa_name, xml_path):
    """Parse image info from proteinatlas.xml and return in a dict.
    
    This function parses info for one image. To parse efficiently for
    multiple hpa_names, use get_img_info function.

    Args:
        hpa_name: The Human Protein Atlas name for the image.
        xml_path: The full path to the XML file containing image metadata.

    Returns:
        A dict containing relevant info if found,
        otherwise None. E.g.:

        {'hpa_name': 'F_342_9',
         'cell_line': 'U-251 MG',
         'verification': 'approved',
         'locations': ['nucleoplasm', 'cytosol'],
         'img_url': 'http://www.proteinatlas.org/path/to/img.jpg'}
    """
    cell_expr = get_expr_elem(hpa_name, xml_path)
    img_info = None
    if cell_expr is not None:
        img_info = extract_img_info(cell_expr, hpa_name)
    
    return img_info


def extract_img_info(elem, hpa_name):
    """Find the relevant fields from the XML element.
    
    Args:
        elem: An xml.etree.ElementTree.Element containing the data.
        hpa_name: The Human Protein Atlas name of the image.

    Returns: A dict containing the info, None otherwise.
    """
    ver = elem.find('.//verification')
    if ver is not None:
        ver = ver.text
    
    cell_line = None
    locations = []
    img_url = None
    for data in elem.findall('.//data'):
        for url in data.findall('.//imageUrl'):
            if '/' + hpa_name + '_' in url.text:
                img_url = url.text
                cl = data.find('.//cellLine')
                if cl is not None:
                    cell_line = cl.text
                locations = [loc.text for loc in data.findall('.//location')]
                
    return {
        "hpa_name": hpa_name,
        "cell_line": cell_line,
        "verification": ver,
        "locations": locations,
        "img_url": img_url}


def get_expr_elem(hpa_name, xml_path):
    """Parse image info from proteinatlas.xml.
    
    This is used only in get_one_img_info. For parsing info
    for multiple hpa_names, use get_img_info function instead of get_one_img_info...
    """
    events = ('start', 'end')
    in_cellexpr = False
    
    for event, elem in ET.iterparse(xml_path, events=events):
        if event == 'start' and elem.tag == 'cellExpression':
            in_cellexpr = True
            
        if event == 'end' and elem.tag == 'cellExpression':
            in_cellexpr = False
            
        if event == 'end' and elem.tag == 'cellExpression':
            for im_elem in elem.findall('.//imageUrl'):
                if '/' + hpa_name + '_' in im_elem.text:
                    print("Found imageUrl matching {}".format(hpa_name))
                    return copy.copy(elem)
                    
        if in_cellexpr == False:
            elem.clear()
            
    # If no matching expression element was found, return None
    return None

def get_img_info(hpa_names, xml_path):
    """Parse image info from proteinatlas.xml for all names in hpa_names.
    
    Args:
        hpa_names: A list of Human Protein Atlas names for the images, e.g.
            ['565_F9_1', '812_C9_2']
        xml_path: The full path to the XML file containing the data.
    
    Returns:
        A dict containing the info for each HPA name. E.g.:

        {'565_F1_1': {'cell_line': 'U-251 MG',
            'verification': 'approved',
            'locations': ['nucleoplasm', 'cytosol'],
            'img_url': 'http://www.proteinatlas.org/path/to/img.jpg'}
         '812_C9_2': {'cell_line': 'U-251 MG',
            'verification': 'approved',
            'locations': ['nucleoplasm', 'cytosol'],
            'img_url': 'http://www.proteinatlas.org/path/to/img.jpg'}}

    Raises:
        ValueError: More than one HPA name matched to a single image's url.
            This should not happen, there's error in matching code.
    """
    events = ('start', 'end')
    hpa_name_set = set(hpa_names)
    # A dict where hpa_name is the key and value is dict of info
    img_info_dict = {}
    in_cellexpr = False
    
    # Here, we use ElementTree.iterparse because we don't want
    # to read the whole XML file into memory (the file is about
    # 8G, and it would be probably four times more when loaded into
    # ElementTree... We track the start and end events of the elements,
    # so that we can load the whole cellExpression element's subtree
    # into memory. When the whole subtree has been traversed, we 
    # get rid off it to free memory.
    for event, elem in ET.iterparse(xml_path, events=events):
        if event == 'start' and elem.tag == 'cellExpression':
            in_cellexpr = True
            
        if event == 'end' and elem.tag == 'cellExpression':
            in_cellexpr = False
            
        if event == 'end' and elem.tag == 'cellExpression':
            for img_url in elem.findall('.//imageUrl'):
                url_matches = [n for n in hpa_name_set if '/' + n + '_' in img_url.text]
                
                if len(url_matches):
                    if len(url_matches) > 1:
                        raise ValueError(
                            "Image url matched to more than one hpa name: {} matches. ImgUrl: {} matches: {}".format(
                                str(len(url_matches)),
                                img_url.text,
                                str(url_matches)
                            )
                        )
                    
                    hpa_name = url_matches[0]
                    img_info = extract_img_info(elem, hpa_name)
                    # Remove the key from result dict
                    img_info.pop('hpa_name')
                    img_info_dict[hpa_name] = img_info
                    # Remove the name from the set
                    hpa_name_set - {hpa_name}
        
        # This will get rid off the element in memory
        if in_cellexpr == False:
            elem.clear()
            
    # If no matching expression element was found, return None
    return img_info_dict


def generate_img_info_csv(cyto_names, hpa_names, xml_path, csv_path, gt_path):
    """Get image info and save results as csv.
     
    Args:
        cyto_names: A list containing all the images prefixes in
            the cyto2017 datasets. These are just integers.
        hpa_names: A list containing all the HPA names corresponding the
            cyto_names. These need to be in the same order in the list.
        xml_path: The full path into the XML file containing the data on images.
        csv_path: The full path into the csv file to be generated.
    """
    # Get img info for each hpa_name
    img_info = get_img_info(hpa_names, xml_path)
    img_data = []
    
    for cyto_name, hpa_name in zip(cyto_names, hpa_names):
        # Create a list of dicts with mapping and img info
        try:
            d = img_info[hpa_name]
        except KeyError:
            d = {"cell_line": None, "verification": None, "locations": [],"img_url": None}
        d['cyto_name'] = cyto_name
        d['hpa_name'] = hpa_name
        img_data.append(d)
    
    # Create a pd.DataFrame from img_data
    df = pd.DataFrame(img_data)

    # Add Cyto challenge labels to the dataframe
    with open(gt_path) as f:
        lines = f.readlines()
        cyto_labels = {l[0]: l[1:] for l in map(lambda s: s.strip().split(sep=', '), lines)}
    df['cyto_labels'] = df['cyto_name'].apply(lambda n: cyto_labels[str(n)])

    df.to_csv(csv_path, sep='\t')


def show_image_info(cyto_name, csv_path, img_dir):
    """Show the image and info about it.
    
    Args:
        cyto_name: The HPA name of the image.
        csv_path: The full path into the CSV file generated by
            generate_img_info_csv function.
        img_dir: The full path to the directory which should contain the
            corresponding images to show.
    """
    df = pd.read_csv(csv_path, sep='\t', index_col=0)
    info = df[df['cyto_name'] == cyto_name]
    print("------Info------")
    fields = ['cyto_name', 'hpa_name', 'cell_line', 'locations', 'verification']
    for f in fields:
        print("\t", f, ": ", info[f].iloc[0])

    show_image(cyto_name, img_dir)


def show_image(cyto_name, img_dir, target_size=(512, 512)):
    """Show the red, green and blue channels of the img."""
    blue = skimage.io.imread(os.path.join(img_dir, '{}_blue.tif'.format(cyto_name)), plugin='tifffile')
    red = skimage.io.imread(os.path.join(img_dir, '{}_red.tif'.format(cyto_name)), plugin='tifffile')
    green = skimage.io.imread(os.path.join(img_dir, '{}_green.tif'.format(cyto_name)), plugin='tifffile')
    
    arr_rgb = np.zeros((blue.shape + (3,)), 'uint8')
    arr_rgb[..., 2] = blue.astype(float)/np.max(blue)*255
    arr_rgb[..., 1] = green.astype(float)/np.max(green)*255
    arr_rgb[..., 0] = red.astype(float)/np.max(red)*255
    print("Original image shape: ", arr_rgb.shape)
    if target_size:
        arr_rgb = skimage_resize(arr_rgb, target_size)
        print("Resized image shape: %s" % str(arr_rgb.shape))

    plt.figure(figsize=(11, 11))
    plt.imshow(arr_rgb)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get image info from proteinatlas.')
    parser.add_argument('--imginfodir', required=True)
    parser.add_argument('--outfile', required=True)
    parser.add_argument('--atlasfile', required=True)
    parser.add_argument('--gtfile', required=True)

    args = parser.parse_args()

    # Get hpa and cyto names
    with open(os.path.join(args.imginfodir, 'hpa_names.txt')) as f:
        hpa_names = [l.strip() for l in f.readlines()]
    with open(os.path.join(args.imginfodir, 'cyto_names.txt')) as f:
        cyto_names = [l.strip() for l in f.readlines()]

    generate_img_info_csv(cyto_names, hpa_names, args.atlasfile, args.outfile, args.gtfile)

