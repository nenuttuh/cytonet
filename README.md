# CytoNet

The code as a python package for my master's thesis.

**Author**: Riku Huttunen riku.huttunen@student.tut.fi

## Architecture

![Cytonet class diagram](./doc/cytonet-class-diagram.svg)
**Fig. 1:** Class diagram for the Cytonet package.

![Testbench sequence diagram](./doc/tb-sequence.svg)
**Fig. 2:** Sequence of picking up and running a test bench.
